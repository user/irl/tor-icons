Tor Project Icons
=================

https://people.torproject.org/~irl/icons/

Tor Project Icons is a font including icons relevant to the Tor Project.

Pre-requisites:

    npm install -g icon-font-generator
    apt install inkscape

To build:

    make

You will then find webfonts in the `dist/` folder along with PNGs at 16x16,
32x32, 64x64 or 128x128 in the `dist/NNxNN` folders.

License
-------

    Copyright (c) 2017, Tor Project and contributors (https://www.torproject.org/),
    with Reserved Font Name Tor Project Icons.
    
    Additionally, some icons are derivatives or copies of Font Awesome icons and
    are included here under the terms of the SIL OFL 1.1.
    See: Font Awesome by Dave Gandy (http://fontawesome.io)
    
    This Font Software is licensed under the SIL Open Font License, Version 1.1.
    This license is copied below, and is also available with a FAQ at:
    http://scripts.sil.org/OFL
